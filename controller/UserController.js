const express = require('express');
const Joi = require('joi');
const validation = require('express-joi-validation').createValidator({});

// get authenToken from AuthService
const { authenToken } = require('../service/AuthService.js');
// get destructure from userService
const {
	getUserInfo,
	delUserInfo,
	updatePassword,
} = require('../service/UserService.js');

const updatePasswordSchema = Joi.object({
	oldPassword: Joi.string().required(),
	newPassword: Joi.string().required(),
});

const router = express.Router();
router.get('/', authenToken, getUserInfo);
router.delete('/', authenToken, delUserInfo);
router.patch(
	'/password',
	authenToken,
	validation.body(updatePasswordSchema),
	updatePassword
);
module.exports = router;
