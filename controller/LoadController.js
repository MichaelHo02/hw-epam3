const express = require('express');

// get destructured from LoadService
const {
	getLoads,
	addLoad,
	getActiveLoads,
	nextLoadState,
	getLoad,
	updateLoad,
	deleteLoad,
	postLoad,
	getLoadShippingInfo,
} = require('../service/LoadService.js');

const { statusEnum } = require('../model/LoadModel.js');

const Joi = require('joi');
const validation = require('express-joi-validation').createValidator({});

const getLoadsSchema = Joi.object({
	status: Joi.string().valid(...statusEnum),
	limit: Joi.number(),
	offset: Joi.number(),
});

const addLoadSchema = Joi.object({
	name: Joi.string().required(),
	payload: Joi.number().required(),
	pickup_address: Joi.string().required(),
	delivery_address: Joi.string().required(),
	dimensions: {
		width: Joi.number().required(),
		length: Joi.number().required(),
		height: Joi.number().required(),
	},
});

const getLoadSchema = Joi.object({
	id: Joi.string().required(),
});

const updateLoadSchemaParams = Joi.object({
	id: Joi.string().required(),
});

const updateLoadSchemaBody = Joi.object({
	name: Joi.string().required(),
	payload: Joi.number().required(),
	pickup_address: Joi.string().required(),
	delivery_address: Joi.string().required(),
	dimensions: {
		width: Joi.number().required(),
		length: Joi.number().required(),
		height: Joi.number().required(),
	},
});

const deleteLoadSchema = Joi.object({
	id: Joi.string().required(),
});

const postLoadSchema = Joi.object({
	id: Joi.string().required(),
});

const getLoadShippingInfoSchema = Joi.object({
	id: Joi.string().required(),
});

const router = express.Router();
router.get('/', validation.query(getLoadsSchema), getLoads);
router.post('/', validation.body(addLoadSchema), addLoad);
router.get('/active', getActiveLoads);
router.patch('/active/state', nextLoadState);
router.get('/:id', validation.params(getLoadSchema), getLoad);
router.put(
	'/:id',
	validation.params(updateLoadSchemaParams),
	validation.body(updateLoadSchemaBody),
	updateLoad
);
router.delete('/:id', validation.params(deleteLoadSchema), deleteLoad);
router.post('/:id/post', validation.params(postLoadSchema), postLoad);
router.get(
	'/:id/shipping_info',
	validation.params(getLoadShippingInfoSchema),
	getLoadShippingInfo
);

module.exports = router;
