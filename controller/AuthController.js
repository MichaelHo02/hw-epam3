const express = require('express');
const Joi = require('joi');
const validation = require('express-joi-validation').createValidator({});

// get enum from UserModel
const { roleEnum } = require('../model/UserModel.js');

// get { register, login } from service/AuthService.js
const { register, login } = require('../service/AuthService.js');

const registerSchema = Joi.object({
	email: Joi.string().email().required(),
	password: Joi.string().required(),
	role: Joi.string()
		.valid(...roleEnum)
		.required(),
});

// create loginSchema
const loginSchema = Joi.object({
	email: Joi.string().email().required(),
	password: Joi.string().required(),
});

const router = express.Router();
router.post('/register', validation.body(registerSchema), register);
router.post('/login', validation.body(loginSchema), login);
module.exports = router;
