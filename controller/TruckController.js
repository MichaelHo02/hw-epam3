const express = require('express');

const Joi = require('joi');
const validation = require('express-joi-validation').createValidator({});

const { Truck, typeEnum, statusEnum } = require('../model/TruckModel.js');

// destructure for truckService.js
const {
	getTrucks,
	addTruck,
	getTruck,
	updateTruck,
	deleteTruck,
	assignTruck,
} = require('../service/TruckService.js');

const { authenToken } = require('../service/AuthService.js');

const addTruckSchema = Joi.object({
	type: Joi.string()
		.valid(...typeEnum)
		.required(),
});

const getTruckSchema = Joi.object({
	id: Joi.string().required(),
});

const updateTruckSchemaParams = Joi.object({
	id: Joi.string().required(),
});

const updateTruckSchemaBody = Joi.object({
	type: Joi.string()
		.valid(...typeEnum)
		.required(),
});

const deleteTruckSchema = Joi.object({
	id: Joi.string().required(),
});

const assignTruckSchema = Joi.object({
	id: Joi.string().required(),
});

const router = express.Router();
router.get('/', authenToken, getTrucks);
router.post('/', validation.body(addTruckSchema), authenToken, addTruck);
router.get('/:id', validation.params(getTruckSchema), authenToken, getTruck);
router.put(
	'/:id',
	validation.params(updateTruckSchemaParams),
	validation.body(updateTruckSchemaBody),
	authenToken,
	updateTruck
);
router.delete(
	'/:id',
	validation.params(deleteTruckSchema),
	authenToken,
	deleteTruck
);
router.post(
	'/:id/assign',
	validation.params(assignTruckSchema),
	authenToken,
	assignTruck
);
module.exports = router;
