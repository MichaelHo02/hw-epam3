const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');
const dotenv = require('dotenv');
const dotenvExpand = require('dotenv-expand');

const authController = require('./controller/AuthController');
const loadController = require('./controller/LoadController');
const truckController = require('./controller/TruckController');
const userController = require('./controller/UserController');

const myEnv = dotenv.config();
dotenvExpand.expand(myEnv);

const URI = process.env.URI;
const PORT = process.env.PORT;

const app = express();
app.use(express.json());
app.use(morgan('combined'));

app.use('/api/users/me', userController);
app.use('/api/trucks', truckController);
app.use('/api/loads', loadController);
app.use('/api/auth', authController);

mongoose
	.connect(URI, {
		useNewUrlParser: true,
		useUnifiedTopology: true,
	})
	.then(() => {
		console.log('Connected to database');
		app.listen(PORT, () => {
			console.log(`Server is running on port ${PORT}`);
		});
	})
	.catch((err) => {
		console.log('Server Error: ', err);
	});
