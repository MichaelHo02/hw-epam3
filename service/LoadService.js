const { Load, stateEnum } = require('../model/LoadModel.js');
const { Truck, typeObj } = require('../model/TruckModel.js');
const { User } = require('../model/UserModel.js');

const getLoads = async (req, res, next) => {
	const { email, role } = req.payload;
	const { status, limit = 10, offset = 0 } = req.query;

	if (!email || !role) {
		return res.status(400).json({
			message: 'Invalid request',
		});
	}

	if (role !== 'SHIPPER') {
		return res.status(400).json({ message: 'Invalid user' });
	}

	try {
		const user = await User.findOne({ email });
		const page = await Load.paginate(
			{ createdBy: user._id, status },
			{ offset, limit }
		);
		return res.status(200).json({
			loads: page.loads,
		});
	} catch (error) {
		return res.status(500).json({ message: 'Internal error' });
	}
};
const addLoad = async (req, res, next) => {
	const { email, role } = req.payload;
	const { name, payload, pickupAddress, deliveryAddress, dimensions } =
		req.body;

	if (!email || !role) {
		return res.status(400).json({
			message: 'Invalid request',
		});
	}

	if (role !== 'SHIPPER') {
		return res.status(400).json({ message: 'Invalid user' });
	}

	try {
		const user = await User.findOne({ email });
		const load = new Load({
			name,
			payload,
			pickupAddress,
			deliveryAddress,
			dimensions,
			createdBy: user._id,
			assignedTo: null,
			status: 'NEW',
			sate: null,
			createdDate: new Date().toISOString(),
		});
		await load.save();
		return res.status(200).json({
			message: 'Load created successfully',
		});
	} catch (error) {
		return res.status(500).json({ message: 'Internal error' });
	}
};
const getActiveLoads = async (req, res, next) => {
	const { email, role } = req.payload;

	if (!email || !role) {
		return res.status(400).json({
			message: 'Invalid request',
		});
	}

	if (role !== 'DRIVER') {
		return res.status(400).json({ message: 'Access denied' });
	}

	try {
		const user = await User.findOne({ email });
		const load = await Load.findOne({
			assignedTo: user._id,
			status: 'ASSIGNED',
		});
		return res.status(200).json({ load });
	} catch (error) {
		return res.status(500).json({ message: 'Internal error' });
	}
};
const nextLoadState = async (req, res, next) => {
	const { email, role } = req.payload;

	if (!email || !role) {
		return res.status(400).json({
			message: 'Invalid request',
		});
	}

	if (role !== 'DRIVER') {
		return res.status(400).json({ message: 'Access denied' });
	}

	try {
		const user = await User.findOne({ email });
		const load = await Load.findOne({
			assignedTo: user._id,
		});
		if (load.state === stateEnum[stateEnum.length - 1]) {
			return res
				.status(400)
				.json({ message: 'Load is already in last state' });
		}
		load.state = stateEnum[stateEnum.indexOf(load.state) + 1];
		await load.save();
		return res.status(200).json({
			message: `Load state changed to ${load.state}`,
		});
	} catch (error) {
		return res.status(500).json({ message: 'Internal error' });
	}
};
const getLoad = async (req, res, next) => {
	const { id } = req.params;

	try {
		const load = await Load.findOne({ _id: id });
		return res.status(200).json({ load });
	} catch (error) {
		return res.status(500).json({ message: 'Internal error' });
	}
};
const updateLoad = async (req, res, next) => {
	const { id } = req.params;
	const { name, payload, pickupAddress, deliveryAddress, dimensions } =
		req.body;

	try {
		const load = await Load.findOne({ _id: id });
		load.name = name;
		load.payload = payload;
		load.pickupAddress = pickupAddress;
		load.deliveryAddress = deliveryAddress;
		load.dimensions = dimensions;
		await load.save();
		return res.status(200).json({
			message: 'Load details changed successfully',
		});
	} catch (error) {
		return res.status(500).json({ message: 'Internal error' });
	}
};
const deleteLoad = async (req, res, next) => {
	const { email, role } = req.payload;
	const { id } = req.params;

	if (!email || !role) {
		return res.status(400).json({
			message: 'Invalid request',
		});
	}

	if (role !== 'SHIPPER') {
		return res.status(400).json({ message: 'Access denied' });
	}

	try {
		const load = await Load.findOne({ _id: id });
		await load.remove();
		return res.status(200).json({
			message: 'Load deleted successfully',
		});
	} catch (error) {
		return res.status(500).json({ message: 'Internal error' });
	}
};
const postLoad = async (req, res, next) => {
	const { email, role } = req.payload;
	const { id } = req.params;

	if (!email || !role) {
		return res.status(400).json({
			message: 'Invalid request',
		});
	}

	if (role !== 'SHIPPER') {
		return res.status(400).json({ message: 'Access denied' });
	}

	try {
		const load = await Load.findOne({ _id: id });
		let type = null;
		for (const key in typeObj) {
			if (Object.hasOwnProperty.call(typeObj, key)) {
				const data = typeObj[key];
				if (
					data.payload >= load.payload &&
					data.dimensions.width >= load.dimensions.width &&
					data.dimensions.height >= load.dimensions.height &&
					data.dimensions.length >= load.dimensions.length
				) {
					type = key;
					break;
				}
			}
		}

		const truck = await Truck.find({
			status: 'IS',
			type,
		});
		if (truck) {
			load.assignedTo = truck._id;
			load.status = 'ASSIGNED';
			load.state = 'En route to Pick Up';
			truck.status = 'OL';
		} else {
			load.status = 'NEW';
		}

		await load.save();
		await truck.save();

		return res.status(200).json({
			message: 'Load assigned successfully',
		});
	} catch (error) {
		return res.status(500).json({ message: 'Internal error' });
	}
};
const getLoadShippingInfo = async (req, res, next) => {
	const { email, role } = req.payload;
	const { id } = req.params;

	if (!email || !role) {
		return res.status(400).json({
			message: 'Invalid request',
		});
	}

	if (role !== 'SHIPPER') {
		return res.status(400).json({ message: 'Access denied' });
	}

	try {
		const load = await Load.findOne({ _id: id });
		const truck = await Truck.findOne({ _id: load.assignedTo });
		return res.status(200).json({
			load,
			truck,
		});
	} catch (error) {
		return res.status(500).json({ message: 'Internal error' });
	}
};

module.exports = {
	getLoads,
	addLoad,
	getActiveLoads,
	nextLoadState,
	getLoad,
	updateLoad,
	deleteLoad,
	postLoad,
	getLoadShippingInfo,
};
