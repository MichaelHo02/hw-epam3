const User = require('../model/UserModel.js');
const bcrypt = require('bcrypt');

const getUserInfo = async (req, res, next) => {
	const { email } = req.payload;
	if (!email) {
		return res.status(400).json({
			message: 'Bad request',
		});
	}
	try {
		const user = await User.findOne({ email });
		return res.status(200).json({
			user: {
				_id: user._id,
				role: user.role,
				email: user.email,
				created_date: user.createdDate,
			},
		});
	} catch (error) {
		return res.status(500).json({
			message: error.message,
		});
	}
};
const delUserInfo = async (req, res, next) => {
	const { email } = req.payload;
	if (!email) {
		return res.status(400).json({
			message: 'Bad request',
		});
	}
	try {
		const result = await User.deleteOne({ email });
		if (result.deletedCount === 0) {
			return res.status(404).json({
				message: 'User not found or cannot delete user',
			});
		}
		return res.status(200).json({ message: 'Success' });
	} catch (error) {
		return res.status(500).json({
			message: error.message,
		});
	}
};
const updatePassword = async (req, res, next) => {
	const { email } = req.payload;
	const { oldPassword, newPassword } = req.body;
	if (!email) {
		return res.status(400).json({
			message: 'Bad request',
		});
	}

	try {
		const user = await User.findOne({ email });
		const result = await bcrypt.compare(oldPassword, user.password);
		if (result) {
			const hashPassword = await bcrypt.hash(newPassword, 10);
			await User.updateOne(
				{ email },
				{
					$set: {
						password: hashPassword,
					},
				}
			);
			return res
				.status(200)
				.json({ message: 'Password changed successfully' });
		} else {
			return res
				.status(400)
				.json({ message: 'Wrong email or password!' });
		}
	} catch (error) {
		return res.status(500).json({
			message: error.message,
		});
	}
};

module.exports = { getUserInfo, delUserInfo, updatePassword };
