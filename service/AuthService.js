const User = require('../model/UserModel.js');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const dotenv = require('dotenv');
const dotenvExpand = require('dotenv-expand');

const myEnv = dotenv.config();
dotenvExpand.expand(myEnv);

const register = async (req, res) => {
	const { email, password, role } = req.body;
	if (!email || !password || !role) {
		return res.status(400).json({
			message: 'Invalid email or password or role',
		});
	}

	try {
		let user = await User.findOne({ email });
		if (!!user) {
			return res.status(400).json({
				message: 'Email already registered',
			});
		}
		bcrypt
			.hash(password, 10)
			.then(async (hashPassword) => {
				const newUser = new User({
					email,
					password: hashPassword,
					createdDate: new Date().toISOString(),
				});
				await newUser.save();
				return res.status(200).json({
					message: 'Profile created successfully',
				});
			})
			.catch(() =>
				res.status(500).json({
					message: 'Internal Error',
				})
			);
	} catch (error) {
		return res.status(500).json({
			message: 'Internal Error',
		});
	}
};

const login = async (req, res) => {
	const { email, password } = req.body;
	if (!email || !password) {
		return res.status(400).json({
			message: 'Invalid email or password',
		});
	}
	try {
		const user = await User.findOne({ email });
		const result = await bcrypt.compare(password, user.password);
		if (result) {
			const accessToken = jwt.sign(
				{ email, role: user.role },
				process.env.ACCESS_TOKEN_SECRET
			);
			return res.status(200).json({
				message: 'Success',
				jwt_token: accessToken,
			});
		} else {
			return res
				.status(400)
				.json({ message: 'Invalid email or password' });
		}
	} catch (error) {
		return res.status(500).json({ message: 'Internal Server Error' });
	}
};

const authenToken = async (req, res, next) => {
	try {
		const authorizationHeader = req.headers['authorization'];
		const token = authorizationHeader.split(' ')[1];
		if (!token)
			return res.send(401).json({
				message: 'No Token',
			});
		jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, data) => {
			if (err) return res.status(403).json({ error: err });
			req.payload = data;
			next();
		});
	} catch (error) {
		return res.status(400).json({ message: 'Invalid token' });
	}
};

module.exports = { register, login, authenToken };
