// import Truck
const { Truck } = require('../model/TruckModel.js');
const { User } = require('../model/UserModel.js');

const getTrucks = async (req, res, next) => {
	const { email, role } = req.payload;
	if (!email || !role) {
		return res.status(400).json({ message: 'Invalid token' });
	}
	if (role !== 'DRIVER') {
		return res.status(403).json({ message: 'Access denied' });
	}
	try {
		const user = await User.findOne({ email });
		if (!user) {
			return res.status(404).json({ message: 'User not found' });
		}
		const trucks = await Truck.find({ createdBy: user._id });
		if (!trucks) {
			return res.status(404).json({ message: 'Trucks not found' });
		}
		return res.status(200).json({ trucks });
	} catch (error) {
		return res.status(500).json({ message: 'Internal server error' });
	}
};
const addTruck = async (req, res, next) => {
	const { email, role } = req.payload;
	const { type } = req.body;
	if (!email || !role) {
		return res.status(400).json({ message: 'Invalid token' });
	}
	if (role !== 'DRIVER') {
		return res.status(403).json({ message: 'Access denied' });
	}
	try {
		const user = await User.findOne({ email });
		if (!user) {
			return res.status(404).json({ message: 'User not found' });
		}
		const truck = new Truck({
			createdBy: user._id,
			assignedTo: null,
			type: type,
			status: 'IS',
			createdDate: new Date().toISOString(),
		});
		await truck.save();
		return res.status(200).json({ message: 'Truck created successfully' });
	} catch (error) {
		return res.status(500).json({ message: 'Internal server error' });
	}
};
const getTruck = async (req, res, next) => {
	const { email, role } = req.payload;
	const { id } = req.params;
	if (!email || !role) {
		return res.status(400).json({ message: 'Invalid token' });
	}
	if (role !== 'DRIVER') {
		return res.status(403).json({ message: 'Access denied' });
	}
	try {
		const user = await User.findOne({ email });
		if (!user) {
			return res.status(404).json({ message: 'User not found' });
		}
		const truck = await Truck.findOne({ _id: id });
		if (!truck) {
			return res.status(404).json({ message: 'Truck not found' });
		}
		return res.status(200).json({ truck });
	} catch (error) {
		return res.status(500).json({ message: 'Internal server error' });
	}
};
const updateTruck = async (req, res, next) => {
	const { email, role } = req.payload;
	const { id } = req.params;
	const { type } = req.body;
	if (!email || !role) {
		return res.status(400).json({ message: 'Invalid token' });
	}
	if (role !== 'DRIVER') {
		return res.status(403).json({ message: 'Access denied' });
	}
	try {
		const user = await User.findOne({ email });
		if (!user) {
			return res.status(404).json({ message: 'User not found' });
		}
		const truck = await Truck.findOne({ _id: id });
		if (!truck) {
			return res.status(404).json({ message: 'Truck not found' });
		}
		truck.type = type;
		await truck.save();
		return res
			.status(200)
			.json({ message: 'Truck details changed successfully' });
	} catch (error) {
		return res.status(500).json({ message: 'Internal server error' });
	}
};
const deleteTruck = async (req, res, next) => {
	const { email, role } = req.payload;
	const { id } = req.params;
	if (!email || !role) {
		return res.status(400).json({ message: 'Invalid token' });
	}
	if (role !== 'DRIVER') {
		return res.status(403).json({ message: 'Access denied' });
	}
	try {
		const user = await User.findOne({ email });
		if (!user) {
			return res.status(404).json({ message: 'User not found' });
		}
		const truck = await Truck.findOne({ _id: id, createdBy: user._id });
		if (!truck) {
			return res.status(404).json({ message: 'Truck not found' });
		}
		await truck.remove();
		return res.status(200).json({ message: 'Truck deleted successfully' });
	} catch (error) {
		return res.status(500).json({ message: 'Internal server error' });
	}
};
const assignTruck = async (req, res, next) => {
	const { email, role } = req.payload;
	const { id } = req.params;
	if (!email || !role) {
		return res.status(400).json({ message: 'Invalid token' });
	}
	if (role !== 'DRIVER') {
		return res.status(403).json({ message: 'Access denied' });
	}
	try {
		const user = await User.findOne({ email });
		if (!user) {
			return res.status(404).json({ message: 'User not found' });
		}
		const truck = await Truck.findOne({ _id: id });
		if (!truck) {
			return res.status(404).json({ message: 'Truck not found' });
		}
		truck.assignedTo = id;
		await truck.save();
		return res.status(200).json({ message: 'Truck assigned successfully' });
	} catch (error) {
		return res.status(500).json({ message: 'Internal server error' });
	}
};

module.exports = {
	getTrucks,
	addTruck,
	getTruck,
	updateTruck,
	deleteTruck,
	assignTruck,
};
