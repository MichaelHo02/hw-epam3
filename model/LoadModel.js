const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

const statusEnum = ['NEW', 'POSTED', 'ASSIGNED', 'SHIPPED'];
const stateEnum = [
	'En route to Pick Up',
	'Arrived to Pick Up',
	'En route to delivery, Arrived to delivery',
];

const schema = new mongoose.Schema({
	createdBy: {
		type: mongoose.SchemaTypes.ObjectId,
		ref: 'users',
		required: true,
	},
	assignedTo: {
		type: mongoose.SchemaTypes.ObjectId,
		ref: 'trucks',
		required: true,
	},
	status: {
		type: String,
		required: true,
		enum: statusEnum,
	},
	state: {
		type: String,
		required: true,
		enum: stateEnum,
	},
	name: {
		type: String,
		required: true,
	},
	payload: {
		type: Number,
		required: true,
	},
	pickupAddress: {
		type: String,
		required: true,
	},
	deliveryAddress: {
		type: String,
		required: true,
	},
	dimensions: [
		{
			width: {
				type: Number,
				required: true,
			},
			length: {
				type: Number,
				required: true,
			},
			height: {
				type: Number,
				required: true,
			},
		},
	],
	logs: {
		message: {
			type: String,
			required: true,
		},
		time: {
			type: String,
			required: true,
		},
	},
	createdDate: {
		type: Date,
		required: true,
	},
});

schema.plugin(mongoosePaginate);

const Load = mongoose.model('loads', schema);
module.exports = { Load, statusEnum, stateEnum };
