const mongoose = require('mongoose');

// create roleEnum
const roleEnum = ['SHIPPER', 'DRIVER'];

const schema = new mongoose.Schema({
	email: {
		type: String,
		required: true,
		unique: true,
	},
	password: {
		type: String,
		required: true,
	},
	createdDate: {
		type: Date,
		required: true,
	},
	role: {
		type: String,
		required: true,
		enum: roleEnum,
	},
});

const User = mongoose.model('users', schema);
module.exports = { User, roleEnum };
