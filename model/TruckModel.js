const mongoose = require('mongoose');

// create enum
const typeEnum = ['SPRINTER', 'SMALL_STRAIGHT', 'LARGE_STRAIGHT'];
const typeObj = {
	SPRINTER: {
		width: 300,
		length: 250,
		height: 170,
		payload: 1700,
	},
	SMALL_STRAIGHT: {
		width: 500,
		length: 250,
		height: 170,
		payload: 2500,
	},
	LARGE_STRAIGHT: {
		width: 700,
		length: 350,
		height: 200,
		payload: 4000,
	},
};
const statusEnum = ['OL', 'IS'];

const schema = new mongoose.Schema({
	createdBy: {
		type: mongoose.SchemaTypes.ObjectId,
		ref: 'users',
		required: true,
	},
	assignedTo: {
		type: mongoose.SchemaTypes.ObjectId,
		ref: 'users',
	},
	type: {
		type: String,
		required: true,
		enum: typeEnum,
	},
	status: {
		type: String,
		required: true,
		enum: statusEnum,
	},
	createdDate: {
		type: Date,
		required: true,
	},
});

const Truck = mongoose.model('trucks', schema);
module.exports = { Truck, typeEnum, statusEnum, typeObj };
